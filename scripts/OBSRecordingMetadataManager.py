# author Marco Dalla Vecchia (marco.dallavecchia@ist.ac.at) and Robert Hauschild (Robert.Hauschild@ist.ac.at) ISTA IOF
# original author	Jeff Mathewson
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# 
# ------------------------------------------------------------
# Main Script Area
# ------------------------------------------------------------

import obspython as obs
import time
from datetime import datetime  
from datetime import timedelta  
import csv
from glob import glob
import os
import math


# Change me if you are using a different amount of plates
Plates_Number = 6
# Update input and output paths!
Input_Full_Path = "D:/ista_projs/obs_scripting_filename_automation/resources/"
Export_Full_Path = "D:/ista_projs/obs_scripting_filename_automation/output/"
# This has to be the same as the OBS video format default!
Video_Extension = "mkv"

# Other constants
Debug_Mode = False
Enabled_Recording = True
Enabled_Streaming = False
Pause_Time = 200
Recording_Start = "None"
Recording_Timer = 0
Recording_End = ""
Time_To_Record = 0
Video_ID = 0
Metadata_Datetime = "None"

# ------------------------------------------------------------
# OBS Script Functions
# ------------------------------------------------------------

def script_defaults(settings):
	global Debug_Mode
	if Debug_Mode: print("Calling defaults")

def script_description():
	global Debug_Mode
	if Debug_Mode: print("Calling description")

	return "<b>OBS Recording Manager</b>" + \
		"<hr>" + \
		"Split Recording video into user define timed clips." + \
		"<br/>" + \
		"Set Start and Stop times for both recording / streaming." + \
		"<br/>" + \
		"Simple Debug Mode to watch how scripts work in OBS." + \
		"<br/>" + \
		"Record and export metadata of recorded samples." + \
		"<br/><br/>" + \
		'Adapted from <a href="https://obsproject.com/forum/resources/obs-recording-manager.659/">original code by Jeff Mathewson</a>' + \
		"<br/>" + \
		"by Marco Dalla Vecchia and Robert Hauschild (ISTA IOF)" + \
		"<hr>"

def script_load(settings):
	global Metadata_Datetime
	global Debug_Mode
	if Debug_Mode: print("Calling Load")

	obs.obs_data_set_bool(settings, "enabled", False)
	Metadata_Datetime = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
	# Set all plate toggles to ON at start
	for plate_index in range(1,7):
		obs.obs_data_set_bool(settings, "cb_p" + str(plate_index), True)

# New function to add metadata fields for each plate
def create_metadata_props(settings, plate_number):
	global Input_Full_Path
	global Debug_Mode
	if Debug_Mode: print("Creating metadata properties")	

	# Aquarium type
	m_aquarium = obs.obs_properties_add_list(settings, "aquarium" + str(plate_number), "Aquarium Type", obs.OBS_COMBO_TYPE_LIST , obs.OBS_COMBO_FORMAT_STRING)
	for type in ['round', 'square']: obs.obs_property_list_add_string(m_aquarium, type, type)

	# Number of animals
	# m_animal_number = obs.obs_properties_add_int(settings, "animal_number" + str(plate_number), "Animal number", 1, 10, 1)
	m_animal_number = obs.obs_properties_add_list(settings, "animal_number" + str(plate_number), "Animal number", obs.OBS_COMBO_TYPE_LIST , obs.OBS_COMBO_FORMAT_INT)
	for num in list(range(1,11,1)): obs.obs_property_list_add_int(m_animal_number, str(num), num)

	# Genotype
	# gRNA and Genotypes will be combined together
	genotypes_grnas = import_txt(Input_Full_Path + "genotypes-grna.txt")
	m_genotype = obs.obs_properties_add_list(settings, "genotype" + str(plate_number), "Genotype - gRNA", obs.OBS_COMBO_TYPE_LIST , obs.OBS_COMBO_FORMAT_STRING)
	for genotype_grna in genotypes_grnas: obs.obs_property_list_add_string(m_genotype, genotype_grna, genotype_grna)

	# gRNA choice
	# gRNA and Genotypes will be combined together
	# m_grna = obs.obs_properties_add_list(settings, "grna" + str(plate_number), "gRNA", obs.OBS_COMBO_TYPE_LIST , obs.OBS_COMBO_FORMAT_STRING)
	# gRNAs = ["1","2","3","4","5","6","7","9","X"]
	# for gRNA in gRNAs: obs.obs_property_list_add_string(m_grna, gRNA, gRNA)

	# Type of mutant
	m_mutant_type = obs.obs_properties_add_list(settings, "mutant_type" + str(plate_number), "Mutant Type", obs.OBS_COMBO_TYPE_LIST , obs.OBS_COMBO_FORMAT_STRING)
	for mutant in ['whole', 'half']: obs.obs_property_list_add_string(m_mutant_type, mutant, mutant)

	# Stage
	m_stage = obs.obs_properties_add_text(settings, "stage" + str(plate_number), "Stage", obs.OBS_TEXT_DEFAULT)

	# Tank number
	m_tank = obs.obs_properties_add_text(settings, "tank" + str(plate_number), "Tank number", obs.OBS_TEXT_DEFAULT)

	# Water depth
	m_water_depth = obs.obs_properties_add_list(settings, "water_depth" + str(plate_number), "Water Depth", obs.OBS_COMBO_TYPE_LIST, obs.OBS_COMBO_FORMAT_INT)
	for depth in [100,180]: obs.obs_property_list_add_int(m_water_depth, str(depth), depth)

	# Import authors list
	authors = import_txt(Input_Full_Path + "authors.txt")
	# Video creator / Author
	# m_video_creator = obs.obs_properties_add_text(settings, "video_creator" + str(plate_number), "Imaged by", obs.OBS_TEXT_DEFAULT)
	m_video_creator = obs.obs_properties_add_list(settings, "video_creator" + str(plate_number), "Imaged by", obs.OBS_COMBO_TYPE_LIST , obs.OBS_COMBO_FORMAT_STRING)
	for author in authors: obs.obs_property_list_add_string(m_video_creator, author, author)

	# Who generated the animal
	# m_animal_creator = obs.obs_properties_add_text(settings, "animal_creator" + str(plate_number), "Animal generated by", obs.OBS_TEXT_DEFAULT)
	m_animal_creator = obs.obs_properties_add_list(settings, "animal_creator" + str(plate_number), "Animal generated by", obs.OBS_COMBO_TYPE_LIST , obs.OBS_COMBO_FORMAT_STRING)
	for author in authors: obs.obs_property_list_add_string(m_animal_creator, author, author)

	# Day of birth
	m_birthday = obs.obs_properties_add_text(settings, "birthday" + str(plate_number), "Date of birth", obs.OBS_TEXT_DEFAULT)

	# Free field for notes
	m_notes = obs.obs_properties_add_text(settings, "notes" + str(plate_number), "Notes", obs.OBS_TEXT_DEFAULT)
 	
	# Empty line spacer
	obs.obs_properties_add_text(settings, 'l_p' + str(plate_number), "\n", obs.OBS_TEXT_INFO)

def script_properties():
	global Debug_Mode
	if Debug_Mode: print("Calling properties")	

	now = datetime(2023,1,1,0,0)
	props = obs.obs_properties_create()

	# Metadata text fields here, create fields for every plate
	obs.obs_properties_add_text(props, 'l_metadata', "EXPERIMENTAL METADATA", obs.OBS_TEXT_INFO)

	global Plates_Number
	for plate_counter in range(1, Plates_Number + 1):
		
		# Plate number as checkbox (cb)
		obs.obs_properties_add_bool(props, "cb_p" + str(plate_counter), 'Plate ' + str(plate_counter))
		
		create_metadata_props(props, plate_counter)

	# Video options here
	obs.obs_properties_add_text(props, 'l_recording','RECORDING SETTINGS', obs.OBS_TEXT_INFO)
	obs.obs_properties_add_int(props, "duration", "Recording Duration (Minutes)", 1, 120, 1)
	st = obs.obs_properties_add_list(props, "start_time", "Record Start Time", obs.OBS_COMBO_TYPE_LIST , obs.OBS_COMBO_FORMAT_STRING)
	obs.obs_property_list_add_string(st, "None", "None")
	et = obs.obs_properties_add_list(props, "end_time", "Record End Time", obs.OBS_COMBO_TYPE_LIST , obs.OBS_COMBO_FORMAT_STRING)
	for x in range(96*3):
		obs.obs_property_list_add_string(st, str(datetime.time(now).strftime( "%I:%M %p")), str(datetime.time(now)))
		obs.obs_property_list_add_string(et, str(datetime.time(now).strftime( "%I:%M %p")), str(datetime.time(now)))
		# now += timedelta(minutes=15)
		now += timedelta(minutes=5)

	obs.obs_properties_add_bool(props, "enabled_stream", "Include Streaming")
	obs.obs_properties_add_bool(props, "debug_mode", "Debug Mode")

	obs.obs_properties_add_text(props, 'l_spacer1', "\n", obs.OBS_TEXT_INFO) # empty line spacer
	obs.obs_properties_add_text(props, 'l_ready','CHECK ENABLED WHEN YOU ARE READY!', obs.OBS_TEXT_INFO)
	obs.obs_properties_add_bool(props, "enabled", "Enabled")
	return props

def script_save(settings):
	global Debug_Mode
	global Export_Full_Path
	global Video_ID
	if Debug_Mode: 
		print("Calling Save")
		print("Current ID: ", retrieve_current_video_id())

	if obs.obs_data_get_bool(settings, "enabled") is True:
		video_id = retrieve_current_video_id()
		Video_ID = video_id
		metadata = collect_metadata(settings)
		if check_plate_ordering(metadata):
			export_metadata_to_csv(metadata, Export_Full_Path + 'ID' + str(Video_ID) + "_" + Metadata_Datetime + '_metadata.csv')
		else:
			if Debug_Mode:
				print("Plates are not ordered!")
	if not obs.obs_frontend_recording_active():
		increase_current_video_id()
	
	if Video_ID < retrieve_current_video_id():
		rename_exported_videos(Export_Full_Path, Video_ID)

	script_update(settings)

def script_unload():
	global Debug_Mode
	if Debug_Mode: print("Calling unload")

	obs.timer_remove(timer_check_recording)
	obs.timer_remove(timer_start_recording)
	

def script_update(settings):
	global Debug_Mode
	if Debug_Mode: print("Calling Update")

	global Enabled_Recording
	global Enabled_Streaming
	global Pause_Time
	global Recording_Start
	global Recording_Timer
	global Recording_End
	global Time_To_Record
	global Export_Full_Path

	if obs.obs_data_get_bool(settings, "enabled") is not Enabled_Recording:
		if obs.obs_data_get_bool(settings, "enabled") is True:
			if Debug_Mode: print("Loading Timer")

			Enabled_Recording = True
			obs.timer_add(timer_check_recording,1000)
		else:
			if Debug_Mode: print("Unloading Timer")

			Enabled_Recording = False
			obs.timer_remove(timer_check_recording)

	if obs.obs_data_get_int(settings, "duration") == 0:
		Recording_Timer = 30 * 60
	else:
		Recording_Timer = obs.obs_data_get_int(settings, "duration") * 60

	Time_To_Record = time.time() + Recording_Timer
	if obs.obs_data_get_string(settings, "start_time") == "" or obs.obs_data_get_string(settings, "start_time") == "None" or obs.obs_data_get_string(settings, "start_time") == obs.obs_data_get_string(settings, "end_time"):
		Recording_Start = "None"
		obs.obs_data_set_bool(settings, "enabled_stream", False)
		Enabled_Streaming = False
	else:
		Recording_Start = obs.obs_data_get_string(settings, "start_time")

	if obs.obs_data_get_string(settings, "end_time") == "":
		Recording_Start = "None"
		obs.obs_data_set_bool(settings, "enabled_stream", False)
		Enabled_Streaming = False
	else:
		Recording_End = obs.obs_data_get_string(settings, "end_time")
	Debug_Mode = obs.obs_data_get_bool(settings, "debug_mode")
	Enabled_Streaming = obs.obs_data_get_bool(settings, "enabled_stream")

	# start_dt = datetime.strptime(Recording_Start, "%H:%M:%S")
	# end_dt = datetime.strptime(Recording_End, "%H:%M:%S")
	# tot_seconds = (end_dt - start_dt).seconds
	# video_takes = math.ceil(tot_seconds / Recording_Timer)
	# print(tot_seconds)
	# print(Recording_Timer)
	# print(video_takes)
# ------------------------------------------------------------
# Functions
# ------------------------------------------------------------

def check_plate_ordering(metadata_dict_list):
	all_plate_numbers = []
	for metadata_dict in metadata_dict_list:
		all_plate_numbers.append(metadata_dict['plate_number'])

	total_plates = len(all_plate_numbers)

	if all_plate_numbers == list(range(1, total_plates+1)):
		return True
	else: return False

def import_txt(file_path):
	with open(file_path, "r") as f:
		item_list = f.read().splitlines()
	return item_list


def timer_check_recording():
	global Debug_Mode
	if Debug_Mode: print("Timer Event: timer_check_recording")

	global Enabled_Recording
	global Enabled_Streaming
	global Pause_Time
	global Recording_Start
	global Recording_Timer
	global Recording_End
	global Time_To_Record
	global Export_Full_Path
	global Video_ID

	Recording_Active = False	
	
	if Enabled_Recording and Recording_Start is not "None":
		if int(Recording_Start[:2]) <= int(Recording_End[:2]):
			if Debug_Mode: print("Normal Time")
			Recording_Active = time.strftime("%H:%M") >= Recording_Start and time.strftime("%H:%M") <= Recording_End
		else:
			if Debug_Mode: print("Backwards Time")
			Recording_Active = not (time.strftime("%H:%M") <= Recording_Start and time.strftime("%H:%M") >= Recording_End)

		if Recording_Active:
			if obs.obs_frontend_recording_active():
				if time.time() >= Time_To_Record:
					if Debug_Mode: print("I'm going to stop recording now")
					obs.obs_frontend_recording_stop()
					obs.timer_add(timer_start_recording, Pause_Time)
					Time_To_Record = time.time() + Recording_Timer
					
			else:
				obs.obs_frontend_recording_start()

			if obs.obs_frontend_streaming_active() is False and Enabled_Streaming is True:
				obs.obs_frontend_streaming_start()		
		else:		
			# HERE IS WHEN ALL THE RECORDINGS STOP
			if obs.obs_frontend_recording_active():
				obs.obs_frontend_recording_stop()
			else:
				if Debug_Mode: print("Sleeping Waiting for timer ",Recording_Start)

			if obs.obs_frontend_streaming_active() and Enabled_Streaming is True:
				obs.obs_frontend_streaming_stop()

def rename_exported_videos(export_path, video_id):
	global Debug_Mode
	global Video_Extension
	if Debug_Mode: print("Renaming created videos ({} format)".format(Video_Extension))

	# find videos to be renamed
	videos_list = sorted([
		video 
		for video in glob(export_path + "*.{}".format(Video_Extension)) 
		if "ID" not in video
		])

	# loop through videos and renamed them
	for video_count_id, video in enumerate(videos_list):
		new_video_path = os.path.join(
			os.path.dirname(video), 
			"ID" + str(video_id) + "_" + \
			os.path.basename(video).replace(".{}".format(Video_Extension), "") + "_" + \
			str(video_count_id + 1) + ".{}".format(Video_Extension)
			)
		os.rename(video, new_video_path)
	if Debug_Mode: print("Renamed {} videos".format(len(videos_list)))

def retrieve_current_video_id():
	global Input_Full_Path
	global Debug_Mode

	with open(Input_Full_Path + 'CURRENT_ID', 'r') as f: # open in read mode to get ID
		video_id = int(f.readline())
	return video_id

def increase_current_video_id():
	global Input_Full_Path
	global Video_ID
	with open(Input_Full_Path + 'CURRENT_ID', 'w') as f: # open in write mode to update ID
		f.write(str(Video_ID + 1))

def timer_start_recording():
	global Debug_Mode
	if Debug_Mode: print("Timer Event: timer_start_recording")

	if obs.obs_frontend_recording_active() is False:
		obs.obs_frontend_recording_start()
		obs.timer_remove(timer_start_recording)

def collect_metadata(settings):
	global Plates_Number
	global Recording_Start
	global Recording_End
	global Recording_Timer
	global Metadata_Datetime
	global Video_ID

	metadata_list = []

	# calculate video takes
	start_dt = datetime.strptime(Recording_Start, "%H:%M:%S")
	end_dt = datetime.strptime(Recording_End, "%H:%M:%S")
	tot_seconds = (end_dt - start_dt).seconds
	video_takes = math.ceil(tot_seconds / Recording_Timer)
	
	# Populate all metadata
	for plate_counter in range(1, Plates_Number + 1):
		if obs.obs_data_get_bool(settings, "cb_p" + str(plate_counter)):
			plate_dict = {
				'date': Metadata_Datetime.split('_')[0],
				'start_time' : str(Recording_Start),
				'end_time' : str(Recording_End),
				'duration_minutes': str(Recording_Timer / 60),
				'video_takes':  video_takes,
				'video_id': Video_ID,
				'plate_number': plate_counter,
				'animal_number': obs.obs_data_get_int(settings, "animal_number" + str(plate_counter)),
				'genotype_grna' : obs.obs_data_get_string(settings, "genotype" + str(plate_counter)),
				# 'grna' : obs.obs_data_get_string(settings, "grna" + str(plate_counter)),
				'mutant_type' : obs.obs_data_get_string(settings, "mutant_type" + str(plate_counter)),
				'stage' : obs.obs_data_get_string(settings, "stage" + str(plate_counter)),
				'tank_number' : obs.obs_data_get_string(settings, "tank" + str(plate_counter)),
				'birthday' : obs.obs_data_get_string(settings, "birthday" + str(plate_counter)),
				'animal_creator' : obs.obs_data_get_string(settings, "animal_creator" + str(plate_counter)),
				'video_creator' : obs.obs_data_get_string(settings, "video_creator" + str(plate_counter)),
				'script_time': Metadata_Datetime.split('_')[1].replace("-", ":"),
				'water_depth' : obs.obs_data_get_int(settings, "water_depth" + str(plate_counter)),
				'aquarium_type': obs.obs_data_get_string(settings, "aquarium" + str(plate_counter)),
				'notes' : obs.obs_data_get_string(settings, "notes" + str(plate_counter)),
			}

			metadata_list.append(plate_dict)
	return metadata_list

def export_metadata_to_csv(metadata_list, output_path):
	global Debug_Mode
	if Debug_Mode: print("I/O Event: Metadata exported in", output_path)

	with open(output_path, "w", newline="") as csvfile:
		# Create a CSV writer using the field/column names from first item in metadata list
		writer = csv.DictWriter(csvfile, fieldnames=metadata_list[0].keys())
		
		# Write the header row (column names)
		writer.writeheader()
		
		# Write the data
		for row in metadata_list:
			writer.writerow(row)